class ResultDisplay

    def initialize(store:, search_engine:)
        #rooms
        @unbooked = search_engine.unbooked
        #bookings
        @booked_unfilled = search_engine.booked_unfilled
        @store = store
        @hosts = store.get_hosts
        @rooms = store.get_rooms
        @bookings = store.get_bookings
    end

    def display
        count = 1
        
        unbooked.each do |booking|
            room = find_room_in_bookings(booking)
            host_details = find_host_details(room)

            puts "#{host_details[:host_ref]} : #{host_details[:host_name] }"
            puts "#{host_details[:host_adress]}"
            puts "#{room.ref} is available"
            puts "#{room.capacity} / #{room.capacity} beds available"

            count += 1
            break if count > 9
        end

        if count < 10
            booked_unfilled.each do |booking|
                room = find_room_in_bookings(booking)
                host_details = find_host_details(find_room_in_bookings(booking))

                puts "#{host_details[:host_ref]} : #{host_details[:host_name] }"
                puts "#{host_details[:host_adress]}"
                puts "#{room.ref} is available"
                puts "#{booking.number_of_guests} of #{room.capacity} beds available"

                count += 1
                break if count > 9

            end
        end

    end

    def find_host_details(room)
        host = hosts.select {|host| host.ref == room.host_ref}[0]
        return {host_ref: host.ref, host_name: host.name, host_adress: host.adress,}
    end

    

    def find_room_in_bookings(booking)
        room = rooms.select {|room| room.ref == booking.room_ref}[0]
        return room
    end

    

    attr_accessor :unbooked, :booked_unfilled, :store, :hosts, :rooms, :bookings

end

