require_relative("../store")

RSpec.describe Store do
    let(:store) { Store.new }
    
    describe "#show_hosts" do
        
        it "returns empty array" do
            expect(store.show_hosts.is_a?(Array)).to be(true)
            expect(store.show_hosts.empty?).to be(true)
        end
    end

    describe "#show_rooms" do
        
        it "returns empty array" do
            expect(store.show_rooms.is_a?(Array)).to be(true)
            expect(store.show_rooms.empty?).to be(true)
        end
    end

    describe "#show_bookings" do
        
        it "returns empty array" do
            expect(store.show_bookings.is_a?(Array)).to be(true)
            expect(store.show_bookings.empty?).to be(true)
        end
    end

    describe "#save_host" do
        
        it "adds host into the hosts array" do
            store.save_host(host: "Sansa Stark")
            expect(store.get_hosts[0]).to eq("Sansa Stark")
        end

        it "doesnt overwrite previous entry" do
            store.save_host(host: "Sansa Stark")
            store.save_host(host:"Jamie Lannister")
            expect(store.get_hosts[0]).to eq("Sansa Stark")
            expect(store.get_hosts[1]).to eq("Jamie Lannister")
        end
    end

    describe "#save_room" do
        
        it "adds room into the rooms array" do
            store.save_room(room: "Royal Bedroom")
            expect(store.get_rooms[0]).to eq("Royal Bedroom")
        end

        it "doesnt overwrite previous entry" do
            store.save_room(room: "Royal Bedroom")
            store.save_room(room:"Peasant hole")
            expect(store.get_rooms[0]).to eq("Royal Bedroom")
            expect(store.get_rooms[1]).to eq("Peasant hole")
        end

        describe "#save_booking" do
        
            it "adds booking into the bookings array" do
                store.save_booking(booking: "b#1")
                expect(store.get_bookings[0]).to eq("b#1")
            end
    
            it "doesnt overwrite previous entry" do
                store.save_booking(booking: "b#1")
                store.save_booking(booking:"b#2")
                expect(store.get_bookings[0]).to eq("b#1")
                expect(store.get_bookings[1]).to eq("b#2")
            end
        end

        describe "#organize" do
            before(:example) do
                fill_store
            end
            
            it "removes first item in all three arrays" do
                store.organize
                expect(store.get_hosts).to eq(["b","b", "b"])
                expect(store.get_rooms).to eq(["b","b", "b"])
                expect(store.get_bookings).to eq(["b","b", "b"])
            end
        end

        

        
    end

    
end

def fill_store
    store.save_host(host: "a")
        3.times do |i|
            store.save_host(host: "b")
        end
    
        store.save_room(room: "a")
        3.times do |i|
            store.save_room(room: "b")
        end

        store.save_booking(booking: "a")
        3.times do |i|
            store.save_booking(booking: "b")
        end
end