require_relative "../import/hosts_parser"
require_relative "../import/rooms_parser"
require_relative "../import/bookings_parser"
require_relative "../file_reader"
require_relative "../importer"
require_relative "../store"

RSpec.describe Importer do
    
    describe "#perform" do
        let(:store) { Store.new }
        let(:importer) { Importer.new(
            store: store,
            hosts_filename: "spec/fixtures/hosts-2000.csv",
            rooms_filename: "spec/fixtures/rooms-2000.csv",
            bookings_filename: "spec/fixtures/bookings-2000.csv"
        ) }

        context "before running importer.perform" do

            it "corresponding lists are empty" do
                expect(store.get_hosts.empty?).to be(true)
                expect(store.get_rooms.empty?).to be(true)
                expect(store.get_bookings.empty?).to be(true)
            end
        end
        

        context "after running importer" do
            before(:example) do
                importer.perform
                store.organize
            end

            it "corresponding lists are filled with data" do
                expect(store.get_hosts.size).to eq(6)
                expect(store.get_rooms.size).to eq(6)
                expect(store.get_bookings.size).to eq(6)
            end

            it "data is saved in correct list" do
                expect(store.get_hosts[0]).to respond_to(:name)
                expect(store.get_rooms[0]).to respond_to(:capacity)
                expect(store.get_bookings[0]).to respond_to(:room_ref)
            end
        end




    end
end