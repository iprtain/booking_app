require_relative "../search_engine"
require_relative "../importer"
require_relative "../store"


RSpec.describe SearchEngine do
    
    describe "#get_results" do
        let(:store) {Store.new}
        let(:importer) { Importer.new(
            store: store,
            hosts_filename: "spec/fixtures/hosts-2000.csv",
            rooms_filename: "spec/fixtures/rooms-2000.csv",
            bookings_filename: "spec/fixtures/bookings-2000.csv"
        ) }
        let(:seeker) {SearchEngine.new(store:store)}

        context "before running #get results" do

            it "#get_search returns empty array" do
                expect(seeker.get_search.is_a?(Array)).to be(true)
                expect(seeker.get_search.empty?).to be(true)
            end
        end

        context "after running #get_results" do
            before(:example) do
                importer.perform
                store.organize
                seeker.set_parameters2
                seeker.get_results
            end

            it "filters data and returns search result" do
                expect(seeker.get_search.empty?).to be(false)
            end

            it "saves individual data as struct" do
                expect(seeker.get_search[0].is_a?(Struct)).to be(true)
            end

            it "saved data responds to :ref" do 
                expect(store.get_bookings[0]).to respond_to(:ref)
            end

            it "saved data responds to :room_ref" do
                expect(store.get_bookings[0]).to respond_to(:room_ref)
            end

            it "saved data responds to :start_date" do 
                expect(store.get_bookings[0]).to respond_to(:start_date)
            end

            it "saved data responds to :end_date" do 
                expect(store.get_bookings[0]).to respond_to(:end_date)
            end

            it "saved data responds to :number_of_guests" do 
                expect(store.get_bookings[0]).to respond_to(:number_of_guests)
            end

        end


    end
end