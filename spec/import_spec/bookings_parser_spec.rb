require_relative "../../import/bookings_parser"
require_relative "../../store"

RSpec.describe BookingsParser do

    describe "perform" do
        let(:store) { Store.new }
        let(:parser) { BookingsParser.new(store: store) }
        let(:line) { ["b#2", "room#3", "2020-3-1", "2020-3-5", "1" ] }

        context "before running parser.perform" do

            it "store.get_bookings is empty" do
                expect(store.get_bookings.empty?).to be(true)
            end
        end

        context "after running parser.perform" do
            before(:example) do
                parser.perform(line: line)
            end

            it "parses given line and saves it to store" do
                #parser.perform(line: line)

                expect(store.get_bookings.empty?).to be(false)
                expect(store.get_bookings.size).to eq(1)
            end

            it "stored item is a struct" do
                expect(store.get_bookings[0].is_a?(Struct)).to be(true)
            end

            it "struct has attribute :ref" do 
                expect(store.get_bookings[0]).to respond_to(:ref)
            end

            it "struct has attribute :room_ref" do
                expect(store.get_bookings[0]).to respond_to(:room_ref)
            end

            it "struct has attribute :start_date" do 
                expect(store.get_bookings[0]).to respond_to(:start_date)
            end

            it "struct has attribute :end_date" do 
                expect(store.get_bookings[0]).to respond_to(:end_date)
            end

            it "struct has attribute :number_of_guests" do 
                expect(store.get_bookings[0]).to respond_to(:number_of_guests)
            end
        end

        context "after running parser.perform 2 times" do
            
            it "saves both lines" do
                parser.perform(line: line)
                expect(store.get_bookings.empty?).to be(false)
                expect(store.get_bookings.size).to eq(1)
                parser.perform(line: line)
                expect(store.get_bookings.size).to eq(2)
            end
        end
        
    end

end