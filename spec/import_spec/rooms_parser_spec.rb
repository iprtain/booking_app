require_relative "../../import/rooms_parser"
require_relative "../../store"

RSpec.describe RoomsParser do

    describe "perform" do
        let(:store) { Store.new }
        let(:parser) { RoomsParser.new(store: store) }
        let(:line) { ["#1", "Homer", "2"] }

        context "before running parser" do

            it "store.get_rooms is empty" do
                expect(store.get_rooms.empty?).to be(true)
            end
        end

        context "after running parser" do
            before(:example) do
                parser.perform(line: line)
            end

            it "parses given line and saves it to store" do
                expect(store.get_rooms.empty?).to be(false)
                expect(store.get_rooms.size).to eq(1)
            end

            it "saves item as a struct" do 
                expect(store.get_rooms[0].is_a?(Struct)).to be(true)
            end

            it "struct has attribute :ref" do 
                expect(store.get_rooms[0]).to respond_to(:ref)
            end

            it "struct has attribute :host_ref" do 
                expect(store.get_rooms[0]).to respond_to(:host_ref)
            end

            it "struct has attribute :capacity" do
                expect(store.get_rooms[0]).to respond_to(:capacity)
            end
        end

        context "after running parser 2 times" do
            
            it "saves both lines" do
                parser.perform(line: line)
                expect(store.get_rooms.empty?).to be(false)
                expect(store.get_rooms.size).to eq(1)
                parser.perform(line: line)
                expect(store.get_rooms.size).to eq(2)
            end
        end




    end


end