require "date"
#start_date=yyyy-mm-dd&end_date=yyyy-mm-dd&guests=x

class SearchEngine

    def initialize(store:)
        @bookings = store.get_bookings
        @rooms = store.get_rooms
        @booked_unfilled = []
        @unbooked = []
        @search_parameters = {start_date: 0, end_date: 0, guests: 0}
    end

    #debugging
    def set_parameters
        search_parameters[:start_date] = "2020-01-01"
        search_parameters[:end_date] = "2020-01-03"
        search_parameters[:guests] = "1"
    end

    def set_parameters2
        search_parameters[:start_date] = "2013-06-01"
        search_parameters[:end_date] = "2013-06-07"
        search_parameters[:guests] = "1"
    end

    def get_parameters
        puts "Please enter start date yyyy-mm-dd"
        search_parameters[:start_date] = gets.chomp
        puts "Please enter end date yyyy-mm-dd"
        search_parameters[:end_date] = gets.chomp
        puts "Please enter number of guests"
        search_parameters[:guests] = gets.chomp
    end

    def get_results
        #find_unbooked
        find_booked_unfilled
    end

    def get_search
        booked_unfilled
    end

    def get_unbooked
        unbooked
    end

    

    attr_accessor :search_parameters, :store, :bookings, :rooms, :booked_unfilled, :unbooked

    private

    def get_booking_range(booking)
        (Date.parse(booking.start_date)..Date.parse(booking.end_date)).to_a
    end

    def find_room_capacity(ref)
        rooms.each do |room|
            if room.ref == ref
                return room.capacity
                break
            end
        end
    end

    def find_booked_unfilled
        count = 0
        parameter_startdate = Date.parse(search_parameters[:start_date])
        parameter_enddate = Date.parse(search_parameters[:end_date])
        param_range = (parameter_startdate..parameter_enddate).to_a
        bad = false

        bookings.each do |booking|
            bad = false
            included = false
            
            param_range.each do |date|

                booking_range = get_booking_range(booking)
                if booking_range.include?(date)
                    included = true
                    if search_parameters[:guests].to_i > (find_room_capacity(booking.room_ref).to_i - booking.number_of_guests.to_i)
                        bad = true
                        break
                    
                    end
                end
            end
            
            booked_unfilled.push(booking) if bad == false and included == true
            unbooked.push(booking) if included == false
        end
    end
end


=begin
#edgecase, if there can be rooms that are not listed inside of booking.csv

    def find_unbooked
        rooms.each do |room|
            not_booked = true
            bookings.each do |booking|
                if room.ref == booking.room_ref
                    #puts "taken"
                    not_booked = false
                end
            end
            unbooked.push(room) if unbooked == true
        end
    end

end

    ]
=end