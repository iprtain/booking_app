require_relative "import/hosts_parser"
require_relative "import/rooms_parser"
require_relative "import/bookings_parser"
require_relative "file_reader"


class Importer
    
    def initialize(store:, hosts_filename:, rooms_filename:, bookings_filename:)
        @hosts_reader = FileReader.new(filename: hosts_filename, parser: HostsParser.new(store:store))
        @rooms_reader = FileReader.new(filename: rooms_filename, parser: RoomsParser.new(store:store))
        @bookings_reader = FileReader.new(filename: bookings_filename, parser: BookingsParser.new(store:store))
    end

    def perform
        hosts_reader.perform
        rooms_reader.perform
        bookings_reader.perform
    end

    private

    attr_accessor :hosts_reader, :rooms_reader, :bookings_reader

end
