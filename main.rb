require_relative "store"
require_relative "importer"
require_relative "search_engine"
require_relative "result_display"


store = Store.new


importer = Importer.new(
    store:store, hosts_filename:"import/hosts-2000.csv", 
    rooms_filename:"import/rooms-2000.csv", 
    bookings_filename:"import/bookings-2000.csv"
    )
importer.perform
store.organize

#store.show_hosts
#store.show_rooms
#store.show_bookings

seeker = SearchEngine.new(store:store)

#seeker.get_parameters
seeker.set_parameters2
seeker.get_results

#puts seeker.get_unbooked[0]
#puts seeker.get_unbooked[1]
#puts seeker.get_unbooked[2]
#puts seeker.get_search
display = ResultDisplay.new(store:store, search_engine:seeker)

display.display
