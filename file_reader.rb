require "csv"

class FileReader

    def initialize(filename:, parser:)
        @file = filename
        @parser = parser
    end

    def perform
        CSV.foreach(file) do |line|
            parser.perform(line: line)
            #puts line[1]
            #puts line
            #puts "-----"
            #parser.perform(line: line)
        end
    end

    attr_accessor :file, :parser

end