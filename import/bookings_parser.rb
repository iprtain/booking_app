class BookingsParser
    
    def initialize(store: store)
        @store = store
        @booking = Struct.new(
            :ref, :room_ref, :start_date, :end_date, :number_of_guests, keyword_init: true
        )
    end

    def perform(line:)
        store.save_booking(
            booking: booking.new(
            ref: line[0], room_ref:line[1], start_date:line[2], 
            end_date: line[3], number_of_guests:line[4]
            )
        )
    end

    private

    attr_accessor :store, :booking

end