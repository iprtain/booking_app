class RoomsParser
    
    def initialize(store: store)
        @store = store
        @room = Struct.new(:ref, :host_ref, :capacity, keyword_init: true)
    end

    def perform(line:)
        store.save_room(
            room: room.new(
            ref: line[0], host_ref:line[1], capacity:line[2]
            )
        )
    end

    private

    attr_accessor :store, :room

end