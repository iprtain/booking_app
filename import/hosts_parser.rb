class HostsParser
    
    def initialize(store: store)
        @store = store
        @host = Struct.new(:ref, :name, :adress, keyword_init: true)
    end

    def perform(line:)
        store.save_host(
            host: host.new(
            ref: line[0], name:line[1], adress:line[2]
            )
        )
    end

    private

    attr_accessor :store, :host

end