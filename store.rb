class Store

    def initialize
        @hosts = []
        @rooms = []
        @bookings = []
    end

    def save_host(host:)
        hosts.push(host)
    end

    def save_room(room:)
        rooms.push(room)
    end

    def save_booking(booking:)
        bookings.push(booking) 
    end

    def show_hosts
        hosts.each do |host|
            puts "#{host.ref}, #{host.name}, #{host.adress} "
        end
    end

    def show_rooms
        rooms.each do |room|
            puts "#{room.ref}, #{room.host_ref}, #{room.capacity} "
        end
    end

    def show_bookings
        bookings.each do |booking|
            puts "#{booking.ref}, #{booking.room_ref}, #{booking.start_date}, #{booking.end_date}, #{booking.number_of_guests}"
        end
    end

    def get_bookings
        return bookings
    end

    def get_rooms
        return rooms
    end

    def get_hosts
        return hosts
    end

    def organize
        hosts.shift
        rooms.shift
        bookings.shift
    end

    
    private

    attr_accessor :hosts, :rooms, :bookings
    
end